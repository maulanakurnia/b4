# Assigment B4 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin

Position : Intern Backend

This repository contains B4 assignment that include:

1.[E.R.D.](https://gitlab.com/maulanakurnia/b4/-/tree/main/erd)

2.[Supabase](https://gitlab.com/maulanakurnia/b4/-/tree/main/supabase)

3.[Supabase Vue](https://gitlab.com/maulanakurnia/b4/-/tree/main/supabase-vue)

4.[User Story](https://gitlab.com/maulanakurnia/b4/-/tree/main/user-story)
