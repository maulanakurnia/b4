# Supabase

## Screenshot

### Supabase Database
![Supabase Database](screenshot/superbase-database.png)

### Supabase Table
![Supabase Table](screenshot/superbase-table.png)

### Supabase Policies
![Supabase Policies](screenshot/superbase-policies.png)
