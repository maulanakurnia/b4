# Supabase - Vue

## Screenshot

### Login

![Login](screenshot/login.png)

### Register

![Register](screenshot/register.png)

### Create

![Create](screenshot/create.png)

### Update

![Update](screenshot/update.png)

### Tabel Workouts

![Table Workouts](screenshot/table-workouts.png)

### Data Worksouts

![Data Worksouts](screenshot/data-worksout.png)

### Policies

![Policies](screenshot/policies.png)