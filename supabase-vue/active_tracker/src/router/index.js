import { createRouter, createWebHistory } from 'vue-router'
import { supabase } from "../supabase/init";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: () => import("../views/HomeView.vue"),
      meta: {
        title: 'Home',
        auth: false
      }
    },
    {
      path: "/login",
      name: "Login",
      component: () => import("../views/LoginView.vue"),
      meta: {
        title: "Login",
        auth: false
      }
    },
    {
      path: "/register",
      name: "Register",
      component: () => import("../views/RegisterView.vue"),
      meta: {
        title: "Register",
        auth: false,
      },
    },
    {
      path: "/create",
      name: "Create",
      component: () => import("../views/CreateView.vue"),
      meta: {
        title: "Create",
        auth: true,
      },
    },
    {
      path: "/view-workout/:workoutId",
      name: "View-Workout",
      component: () => import("../views/ViewWorkout.vue"),
      meta: {
        title: "View Workout",
        auth: false,
      },
    },
  ]
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} | Active Tracker`;
  next();
});

const auth = async(to, from, next) => {
  const res = await supabase.auth.getUser();
  if (to.matched.some((r) => r.meta.auth)) {
      if (res.data.user) {
        next();
        return;
      }
      next({ name: "Login" });
      return;
    }
    next();
}

// Route guard for auth routes
router.beforeEach((to, from, next) => auth(to, from, next))

export default router