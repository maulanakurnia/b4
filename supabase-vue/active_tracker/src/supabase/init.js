import { createClient } from "@supabase/supabase-js";

// const supabaseUrl = 'https://auktqraarxzziersepme.supabase.co';
// const supabaseAnonKey = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYzMjE3ODYwOCwiZXhwIjoxOTQ3NzU0NjA4fQ.z1WdA8nfYfpju48ojyL6e-IPr3dcmjSH-dQkzpOYH1M';

const supabaseUrl = import.meta.env.VITE_VUE_APP_SUPABASE_URL
const supabaseAnonKey = import.meta.env.VITE_VUE_APP_SUPABASE_ANON_KEY

const supabase = createClient(supabaseUrl, supabaseAnonKey);

export { supabase, supabaseAnonKey, supabaseUrl}